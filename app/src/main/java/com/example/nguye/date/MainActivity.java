package com.example.nguye.date;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    EditText oneday,towday;
    Button btnkq;
    TextView kq;
    Calendar one,tow;
    SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        process();
        simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
        oneday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ngay1();
            }
        });
        towday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ngay2();
            }
        });
        action();
    }

    private void action() {
        btnkq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ngay= (int)( (tow.getTimeInMillis() - one.getTimeInMillis())/(1000*60*60*24));
                Toast.makeText(getApplicationContext(),"số ngày:"+ ngay,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void ngay1() {
        one=Calendar.getInstance();
        int ngay=one.get(Calendar.DATE);
        int thang=one.get(Calendar.MONTH);
        int nam=one.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                one.set(year,month,dayOfMonth);
                oneday.setText(simpleDateFormat.format(one.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }
    private void ngay2() {
        tow=Calendar.getInstance();
        int ngay=tow.get(Calendar.DATE);
        int thang=tow.get(Calendar.MONTH);
        int nam=tow.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog=new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                tow.set(year,month,dayOfMonth);
                towday.setText(simpleDateFormat.format(tow.getTime()));
            }
        },nam,thang,ngay);
        datePickerDialog.show();
    }

    private void process() {
        oneday=findViewById(R.id.edoneday);
        towday=findViewById(R.id.edtowday);
        btnkq=findViewById(R.id.btnkq);
        kq=findViewById(R.id.kq);
    }
}
